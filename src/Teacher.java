
public class Teacher {
	
	private String name;
	private String major;
	private int date;
	
	public Teacher(String name, String major) {
		this.name = name;
		this.major = major;
		date = 5;
	}
	
	public Teacher() {
		// TODO Auto-generated constructor stub
	}

	public String getName(){	
		return name;
		
	}
	
	public String getMajor(){	
		return major;
		
	}
	
	public int getDate(){
		return date;
	}
	
	
}

