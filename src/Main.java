
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Library library = new Library();
		
		Student stu = new Student("Belle", "5610404525");
		Teacher tea = new Teacher("Churerat","ComSci");
		People peo = new People("Mak","TA");
		
		Book book1 = new Book("Java");
		ReferenceBook refbook1 = new ReferenceBook("History");
		
		library.addBook(book1);
		library.addRefBook(refbook1);
		
		
		System.out.println("Name: "+stu.getName());
		System.out.println("Id: "+stu.getId());
		System.out.println("Valid date for Student: "+library.BookCount(stu));
		System.out.println("Name: "+tea.getName());
		System.out.println("Major: "+tea.getMajor());
		System.out.println("Valid date for Teacher: "+library.BookCount(tea));
		System.out.println("Name: "+peo.getName());
		System.out.println("Department: "+peo.getDepartment());
		System.out.println("Valid date for People: "+library.BookCount(peo));
	}

}
